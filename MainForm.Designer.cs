﻿
namespace InternalSort
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radixSortTable = new System.Windows.Forms.DataGridView();
            this.sortButton = new System.Windows.Forms.Button();
            this.sortingData = new System.Windows.Forms.TextBox();
            this.sortLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.radixSortTable)).BeginInit();
            this.SuspendLayout();
            // 
            // radixSortTable
            // 
            this.radixSortTable.AllowUserToAddRows = false;
            this.radixSortTable.AllowUserToDeleteRows = false;
            this.radixSortTable.AllowUserToResizeColumns = false;
            this.radixSortTable.AllowUserToResizeRows = false;
            this.radixSortTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.radixSortTable.Location = new System.Drawing.Point(12, 12);
            this.radixSortTable.Name = "radixSortTable";
            this.radixSortTable.ReadOnly = true;
            this.radixSortTable.RowTemplate.Height = 25;
            this.radixSortTable.Size = new System.Drawing.Size(776, 367);
            this.radixSortTable.TabIndex = 0;
            // 
            // sortButton
            // 
            this.sortButton.Location = new System.Drawing.Point(693, 407);
            this.sortButton.Name = "sortButton";
            this.sortButton.Size = new System.Drawing.Size(95, 23);
            this.sortButton.TabIndex = 1;
            this.sortButton.Text = "Сортировать";
            this.sortButton.UseVisualStyleBackColor = true;
            this.sortButton.Click += new System.EventHandler(this.SortClicked);
            // 
            // sortingData
            // 
            this.sortingData.Location = new System.Drawing.Point(12, 407);
            this.sortingData.Name = "sortingData";
            this.sortingData.Size = new System.Drawing.Size(675, 23);
            this.sortingData.TabIndex = 2;
            this.sortingData.KeyUp += new System.Windows.Forms.KeyEventHandler(this.PressedEnter);
            // 
            // sortLabel
            // 
            this.sortLabel.AutoSize = true;
            this.sortLabel.Location = new System.Drawing.Point(12, 386);
            this.sortLabel.Name = "sortLabel";
            this.sortLabel.Size = new System.Drawing.Size(224, 15);
            this.sortLabel.TabIndex = 3;
            this.sortLabel.Text = "Слова для сортировки (через запятую):";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 444);
            this.Controls.Add(this.sortLabel);
            this.Controls.Add(this.sortingData);
            this.Controls.Add(this.sortButton);
            this.Controls.Add(this.radixSortTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Внешняя сортировка";
            ((System.ComponentModel.ISupportInitialize)(this.radixSortTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView radixSortTable;
        private System.Windows.Forms.Button sortButton;
        private System.Windows.Forms.TextBox sortingData;
        private System.Windows.Forms.Label sortLabel;
    }
}

