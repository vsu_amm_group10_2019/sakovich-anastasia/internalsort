﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternalSort
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private static string[] GetWords(string text)
            => text.Split(',').Select(x => x.Trim()).ToArray();

        private static string GetText(params string[] words)
            => string.Join(", ", words);

        private void SortClicked(object sender, EventArgs e)
        {
            sortingData.Enabled = false;
            sortButton.Enabled = false;
            var temp = new VisualStringRadixSort(radixSortTable);
            var array = GetWords(sortingData.Text);
            temp.SortAsync(array)
                .ContinueWith(x => 
                {
                    sortButton.Enabled = true;
                    sortingData.Text = GetText(array);
                    sortingData.Enabled = true;
                    MessageBox.Show(this, "Сортировка завершена успешно!");
                }, TaskScheduler.FromCurrentSynchronizationContext())
                .ConfigureAwait(true);
        }

        private void PressedEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && sender == sortingData)
                sortButton.PerformClick();
        }
    }
}
