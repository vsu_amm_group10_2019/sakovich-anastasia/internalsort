﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternalSort
{
    class VisualStringRadixSort
    {
        private readonly DataGridView _dataGridView;
        public int DelayAfterPlace { get; set; } = 300;
        public int DelayAfterSplit { get; set; } = 3000;

        public VisualStringRadixSort(DataGridView dataGridView)
            => _dataGridView = dataGridView;

        private char[] ClearAndInitDataGrid(string[] array)
        {
            char[] sortedUniqueSymbols = array.SelectMany(x => x)
                .Distinct()
                .Append('\0')
                .OrderBy(x => x)
                .ToArray();

            _dataGridView.Columns.Clear();
            _dataGridView.Rows.Clear();
            
            var cellCemplate = new DataGridViewTextBoxCell();
            _dataGridView.Columns.AddRange(Enumerable.Range(0, array.Length + 1).Select(x =>
            {
                var column = new DataGridViewColumn()
                {
                    HeaderText = x > 0 ? x.ToString() : string.Empty,
                    CellTemplate = cellCemplate
                };
                return column;
            }).ToArray());

            _dataGridView.Rows.Add(sortedUniqueSymbols.Length);

            for (var i = 0; i < sortedUniqueSymbols.Length; ++i)
                _dataGridView.Rows[i].Cells[0].Value = sortedUniqueSymbols[i];

            return sortedUniqueSymbols;
        }

        private void ClearWordInTable()
        {
            foreach (DataGridViewRow row in _dataGridView.Rows)
                for (int i = 1; i < row.Cells.Count; ++i)
                    row.Cells[i].Value = null;
        }

        private static int GetRowIndex(char[] sortedUniqueSymbols, char symbol)
            => Array.BinarySearch(sortedUniqueSymbols, symbol);

        private static char GetSymbol(string str, int index)
            => str.Length > index && index > -1 ? str[index] : '\0';

        public async Task SortAsync(string[] array)
        {
            int[] indexies = new int[array.Length];
            int phasesCount = array.Select(x => x.Length).Max();
            char[] sortedUniqueSymbols = ClearAndInitDataGrid(array);
            for (int i = 0; i < phasesCount; ++i)
            {
                int[] columnIndex = new int[sortedUniqueSymbols.Length];
                for (var j = 0; j < array.Length; ++j)
                {
                    char symbol = GetSymbol(array[j], indexies[j]++);
                    int rowIndex = GetRowIndex(sortedUniqueSymbols, symbol);
                    _dataGridView.Rows[rowIndex].Cells[columnIndex[rowIndex]++ + 1].Value = array[j];
                    await Task.Delay(DelayAfterPlace);
                }

                int currentWordIndex = 0;
                for (int j = 0; j < columnIndex.Length; j++)
                {
                    for (int k = 0; k < columnIndex[j]; ++k)
                        array[currentWordIndex++] = (string)_dataGridView.Rows[j].Cells[k + 1].Value;
                }

                await Task.Delay(DelayAfterSplit);
                ClearWordInTable();
            }
            _dataGridView.Columns.Clear();
            _dataGridView.Rows.Clear();
        }
    }
}
